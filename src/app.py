from flask import Flask
import flask
from flask_cors import CORS
import os
from flask import Flask, flash, request, redirect, url_for
from werkzeug.utils import secure_filename

UPLOAD_FOLDER = './uploadedFiles'
ALLOWED_EXTENSIONS = {'wav'}

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
CORS(app)

@app.route("/hello")
def hello_world():
    response = flask.jsonify({'APIResponse': 'Hello world from api'})
    return response


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/upload', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return flask.jsonify({'APIResponse': 'Problème de transmission du fichier'})
    file = request.files['file']
    if file.filename == '':
        flash('No selected file')
        return redirect(request.url)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        return flask.jsonify({'APIResponse': 'Upload bien prit en compte'})
    return flask.jsonify({'APIResponse': 'Type de fichier non prit en compte'})